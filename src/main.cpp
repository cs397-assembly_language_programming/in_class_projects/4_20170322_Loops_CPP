#include <iostream>
using namespace std;

char grade(const int&);

int main() {
	string rpt="y";

	do {

		int score;
		cout<<"\n\tPlease enter score: ";
		cin>>score;

		cout<<"\tYour grade is: "<<grade(score)<<endl;

		cin.get();	//eat endl line
		cout<<"\n  Do you want to continue?  ";
		getline(cin,rpt);

	} while(rpt.at(0)=='y' || rpt.at(0)=='Y');

	system("PAUSE");
	return 0;
}

char grade(const int &score) {
	char grade=' ';
	if (score>100 ||score<0) cout<<"\n\t\t/!\\ERROR: invalid score /!\\ \n";
	else {
		if (score>90) return (grade='A');
		else if (score>80) return (grade='B');
		else if (score>70) return (grade='C');
		else if (score>60) return (grade='D');
		else if (score>0) return (grade='F');


	}
	return grade;
}
